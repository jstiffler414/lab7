/*
 *Jordan Stiffler
 * CPSC 1021, Section 1, F20
 * jstiffl@clemson.edu
 * Nushrat
 */

#include <iostream>
#include <iomanip>
#include <algorithm>
#include <sstream>
#include <string>
#include <cstdlib>

using namespace std;

typedef struct Employee{
	string lastName;
	string firstName;
	int birthYear;
	double hourlyWage;
}employee;

bool name_order(const employee& lhs, const employee& rhs);
int myrandom (int i) { return rand()%i;}

int main(int argc, char const *argv[]) {


	srand(unsigned (time(0)));
	
	// creating an employee array that holds 10 people
    employee arr[10];

    int i = 0;
	// while loop entering the information for all 10 employees into array
    while (i < 10) {
	    cout << "Enter the last name of the employee:  ";
	    cin >> arr[i].lastName;
	    cout << endl << "Enter the first name of the employee:  ";
        cin >> arr[i].firstName;
        cout << endl << "Enter the birth year of the employee:  ";
        cin >> arr[i].birthYear;
        cout <<endl << "Enter the hourly wage of the employee:  ";
        cin >> arr[i].hourlyWage;
        cout << endl;

        i+=1;
    }
	
	// pointers to the begining and the end of the employee array
    employee *begin;
    employee *end;
    begin = arr;
    end = &arr[9] + 1;

	// randomly shuffles the people in the array
	// takes pointer to beginning of array, pointer to end of array,
	// and functor myRandom as parameters
    random_shuffle(begin,end, myrandom);

	// creating a new array printarr that takes the first five peole
	// from the shuffled original array
    employee printarr[5] = {arr[0],arr[1],arr[2],arr[3],arr[4]};

	// pointers to the beginning and to the end of the array to 
	// to be printed
    employee *printbegin;
    employee *printend;
    printbegin = printarr;
    printend = &printarr[4] + 1;

	// sorting print array by last name
	// takes pointer to the beginning of the array, pointer to the end 
	// of the array, and a functor as parameters
    sort(printbegin, printend, name_order);
	
	// for loop printing out all the information for each employee
    for (int j = 0; j < 5; ++j) {
		
		// combining the first and last name of the employee together in a string
        string combinedName = printarr[j].lastName+", "+printarr[j].firstName;
		cout << endl << setw(30) << combinedName << endl;
		cout << setw(30) << printarr[j].birthYear << endl;
		cout << setw(30) << fixed << showpoint << setprecision(2) << printarr[j].hourlyWage << endl;
	}

	return 0;

}
	// function that compares the last names of individals from an array
	// returns true if lhs is less than rhs which means it comes first
	// in alphabetical order. Returns false otherwise
	bool name_order(const employee& lhs, const employee& rhs) {
		if ((lhs.lastName) < (rhs.lastName)) {
			return true;
		}
		return false;
	}
